public with sharing class HP3_DuplicateScreen {
	
	Private string conId='';
	
	public HP3_DuplicateScreen(){
	
		conId=ApexPages.currentpage().getparameters().get('conId');
	}
	
	
	public list<duplicateRecordset> getduplicatesResuls(){
		
		list<id> duplicateRecordSetIds= new list<Id>();
		
		for(duplicateRecordItem duplicateRecordSetId:[select id,duplicateRecordset.id from DuplicateRecordItem where contact_Details__C=:conid ]){
		  duplicateRecordSetIds.add(duplicateRecordSetId.duplicateRecordset.id);
		}
		
		list<duplicateRecordset> listofduplicateRecordset=[select id, name,DuplicateRule.MasterLabel, (select contact_Details__c,contact_Details__r.email,contact_Details__r.name,contact_Details__r.furigana__C,contact_Details__r.phone,contact_Details__r.birthdate from duplicateRecordItems) from DuplicateRecordSet where id in :duplicateRecordSetIds];
	    
	   
	  return listofduplicateRecordset;
	}
    
    
    
}