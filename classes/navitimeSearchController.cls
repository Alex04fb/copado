public with sharing class navitimeSearchController {
 
 public string searchQuery {get;set;}
 public list<contact> conlist{get{
  if(conlist==null)
  		conlist= new list<contact>();
  return conlist;
 }set;}
 
 public string selectedQuery {get;set;}
 
 public void searchforthisAddress(){
  searchQuery=searchQuery+'%';	
  conlist=[select id, name from contact where name like :searchQuery];
  
 }
 
 public pagereference logdetails(){
 	apexpages.addmessage(new apexpages.message(apexpages.severity.info,'Hello there '+selectedQuery));
 	system.debug(selectedQuery);
 	return null;
 }
 
 
    
}