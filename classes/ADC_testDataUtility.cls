@istest

Public with sharing class ADC_testDataUtility{


    public static list<account> createAccounts(integer numberOfAccounts, boolean isInsert){
    
        list<Account> listOfAccounts = new list<Account>();
        
        for(integer i=1;i<=numberOfAccounts; i++){
            
            account acc = new account();
            acc.name = 'Test Account Record'+String.valueof(i);
            acc.billingcity = 'Test Account Record billing city '+String.valueof(i);
            acc.billingstreet = 'Test Account Record billing street '+String.valueof(i);
            acc.billingstate = 'Test Account Record billing state'+String.valueof(i);
            acc.billingCountry = 'Test Account Record billing country'+String.valueof(i);
            acc.billingPostalcode = '123456';
            
            listOfAccounts.add(acc);
        }
        
        if(isInsert){
            return insertAccount(listOfAccounts);
        }
        return listOfAccounts;
        
        
    }
    
    public static list<contact> createContacts(integer numContactsPerAcct, List<Account> listOfAccounts, boolean isInsert){
    
        
        list<contact> listofContacts = new list<contact>();
        integer count=0;
        for(integer i=1;i<=listOfAccounts.size(); i++){
            
            for(integer j=1;j<=numContactsPerAcct;j++){
                count =count+1;
                contact con = new contact();
                con.Accountid = listOfAccounts[i].Id;
                con.firstName = 'Test Record ';
                con.lastName = 'contact '+String.valueof(count);
                con.email ='testRecord'+String.valueof(count)+'@testemail.com';
                con.Mailingcity = 'Test Contact Record Mailing city '+String.valueof(count);
                con.Mailingstreet = 'Test Contact Record Mailing street '+String.valueof(count);
                con.Mailingstate = 'Test Contact Record Mailing state'+String.valueof(count);
                con.MailingCountry = 'Test Contact Record Mailing country'+String.valueof(count);
                con.MailingPostalcode = '123456';
                
                listofContacts.add(con);
            
            }
        }
        if(isInsert){
            return insertContacts(listofContacts);
        }
        return listofContacts;
        
        
    }
    public static list<case> createCases(integer numCasesPerContct, List<contact> listOfContacts, boolean isInsert){
    
        
        list<case> listofcases = new list<case>();
        integer count=0;
        for(integer i=1;i<=listOfContacts.size(); i++){
            
            for(integer j=1;j<=numCasesPerContct;j++){
                count =count+1;
                case c = new case();
                c.Accountid = listOfContacts[i].AccountId;
                c.Contactid = listOfContacts[i].Id;
                c.Subject = 'Test case  ' + String.valueof(count);
                c.Description = 'this is Description for test case Record '+String.valueof(count);
                c.status = 'ドラフト';
                c.Origin = 'Web';
                c.Priority='高';
                listofcases.add(c);
            
            }
        }
        if(isInsert){
            return insertCases(listofcases);
        }
        return listofcases;
        
        
    }
    
    public static list<string> testBodiesforEmailRender(list<String> mergeFields){
        
        list<string> li= new list<string>();
        string statement='This is record with below merge fields ';
        for(String mergeField : mergeFields){
          statement += ' AND '+ mergeField; 
        } 
        return li;
        
    }
    
    public static list<account> insertAccount(List<Account> listOfAccounts){
        
        insert listOfAccounts;
        return listOfAccounts;
    }
    
    public static list<Contact> insertContacts(List<Contact> listofContacts){
        
        insert listofContacts;
        return listofContacts;
    }
    
    public static list<case> insertcases(List<case> listofcases){
        
        insert listofcases;
        return listofcases;
    }

}