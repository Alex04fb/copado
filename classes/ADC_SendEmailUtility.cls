/**
* @author Abhilash Mishra
* @date 218/06/2018
*
* @group CandidateMangemnt
* 
*
* @description a library class which will be used to render email templates created in cutom settings
*/

public with sharing class ADC_SendEmailUtility{
    
       
    /*******************************************************************************************************
    * @description To render the data and Merged fields as a email template Body. This method cosumes 2 SOQL per record
    * @param whoId   the Id of person related to email (Lead, Contact, User)
    * @param whatId  the Id of Record related to email
    * @param bodies  list of string to be rendered as template
    * @return Listof string merged body (where merged field are replaced record details)
    * @example
    * 
    */
    
    public static List<Messaging.RenderEmailTemplateBodyResult> ADC_renderEmailTemplate(string whoId, String whatID, list<String> bodies){
        
        //rendering Email templates with what and who Ids
        
        List<Messaging.RenderEmailTemplateBodyResult> emailRenderResults = Messaging.renderEmailTemplate(whoId, whatId, bodies);    
        return emailRenderResults;
    }
    
    /*******************************************************************************************************
    * @description 
    * @param 
    * @return 
    * @example
    * 
    */
    
    public static List<string> ADC_getEmailTemplate(string emailTemplateName){
        
        
        //Querying email Template from custom metadata type ;
        
        List<string> bodies    = new List<string>();
        emailTemplateName=string.escapeSingleQuotes(emailTemplateName);
        Email_Templates__mdt emailTemplate=Database.Query('SELECT ADC_Email_Subject__C, ADC_Email_body__C FROM Email_Templates__mdt where DeveloperName=:emailTemplateName');
        bodies.add(emailTemplate.ADC_Email_Subject__C);    
        bodies.add(emailTemplate.ADC_Email_body__C );    
        
        return bodies;
    }
    
    
    /*******************************************************************************************************
    * @description this methods returns the insatance of singleEmailMessage related to record
    * @param sentTo  list of to addresses
    * @param ccTo    list of cc addresses
    * @param bccTo   list of bcc addresses
    * @param whatId  the Id of Record related to email
    * @param whoId   the Id of person related to email (Lead, Contact, User)
    * @param subject Emal Subject
    * @param body    Email body
    * @return instance of singleEmail Message
    * @example
    * 
    *
    */
    
    public static Messaging.singleEmailMessage ADC_SingleEmailMessage(List<string> sentTo, List<string> ccTo, string whatId, string whoId, string subject,string body){
        
        Messaging.singleEmailMessage emailMessage = new Messaging.singleEmailMessage();
        
        system.debug('To Email Addresses ' + sentTo);
        
        emailMessage.ToAddresses = sentTo;
        emailMessage.ccAddresses = ccTo;
        emailMessage.Subject = subject;
        emailMessage.HtmlBody = body;
        emailMessage.TreatBodiesAsTemplate = true;
        emailMessage.TargetObjectId = whoId;
        emailMessage.TreatTargetObjectAsRecipient = true;
        emailMessage.WhatId = whatId;
        emailMessage.SaveAsActivity =true;
        
        
        return emailMessage;
    }
    
    
    /*******************************************************************************************************
    * @description to create custom sendEmail object instance to insert data
    * @param 
    * @return 
    * @example
    * 
    *
    */
    
    public static EmailMessage ADC_EmailMessage(List<String> sentTo, String subject,String Body){
        
        /*EmailMessage message = new EmailMessage();
            
        message.ToAddress   = (sentTo == null || sentTo.isEmpty()) ? '' : String.join(sentTo,';');
        message.CcAddress   = (ccTo == null || ccTo.isEmpty()) ? '' : String.join(ccTo,';');
        message.BccAddress  = (bccTo == null || bccTo.isEmpty()) ? '' : String.join(ccTo,';');
        message.Subject     = subject;
        message.HtmlBody    = body;
        message.parentId    = parentId;
        message.status      = '0';  //this represent status = new
        
        return message;
        
        */
        
        return null;
            
    }
    
    /*******************************************************************************************************
    * @description to send email using salesforce sendEmail Functionality
    * @param listOfEmailMessages list of single email messages
    * 
    *
    */
    public static void ADC_sendEmail(List<Messaging.singleEmailMessage> listOfEmailMessages){
        
        if(!listOfEmailMessages.isEmpty()){
            system.debug('sending emails. No of emails = '+listOfEmailMessages.size());
            
            try{
                Messaging.sendEmail(listOfEmailMessages);
                
                system.debug(Limits.getEmailInvocations() + 'used out of  '+LImits.getLimitEmailInvocations() );
                
            }catch(Exception e){
                System.debug(e);
            }
        }
    }
    
    /*******************************************************************************************************
    * @description insert data in custom Send email oject
    * @param listOfEmailMessages list of single email messages
    * 
    *
    */
    public static void ADC_createCustomSendEmailRecords(List<EmailMessage> listOfEmailMessages){
        
        
        
    }
    
    
    
    /*******************************************************************************************************
    * @description insert data in custom Send email oject
    * @param listOfEmailMessages list of single email messages
    * 
    *
    */
    
    public static void createCustomEmailRecords(List<sObject> listOfSobjectRecords, String targetObjectIdField, String targetEmailFields,String templateName){
        
        List<string> bodies = new List<String>();
        List<EmailMessage> emailMessage = new List<EmailMessage>();
        String whatId   = '';
        String whoId    = '';
        String subject  = '';
        String body     = '';
        List<string> sentTo= new List<String>();
        
        //gettting email template details
        
        bodies    =  ADC_getEmailTemplate(templateName);
        
        for(sObject sobjectRecord : listOfSobjectRecords){
            
            sentTo= new List<String>();
            whatId = (string)sobjectRecord.get('Id');
            whoId = (string)sobjectRecord.get(targetObjectIdField);
            
            List<Messaging.RenderEmailTemplateBodyResult> EmailRenderResults = ADC_renderEmailTemplate(whoId, whatId, bodies);
            
            System.debug('Email render Subject Error '+EmailRenderResults[0].getErrors());
            System.debug('Email render body Error '+EmailRenderResults[1].getErrors());
            
            subject = EmailRenderResults[0].getMergedBody();
            body    = EmailRenderResults[1].getMergedBody();
            
            Set<string> setOfEmailFieds= new Set<String>(targetEmailFields.split(';'));
            
            for(string emailField:setOfEmailFieds){
                sentTo.add((string)sobjectRecord.get(emailField) );
            }
            
            emailMessage.add(ADC_EmailMessage(sentTo, subject, body));
        }
        
        ADC_createCustomSendEmailRecords(emailMessage);
        
        
        
    }
  
}