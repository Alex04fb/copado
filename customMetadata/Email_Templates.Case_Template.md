<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Case Template</label>
    <protected>false</protected>
    <values>
        <field>ADC_Email_Body__c</field>
        <value xsi:type="xsd:string">Hi {!Contact.name}, &lt;br/&gt; 


your inquiry has been registered. 
&lt;br/&gt; 
Inquiry No : {!case.casenumber} &lt;br/&gt; 
Description : {!case.Description} &lt;br/&gt; 

Thanks&lt;br&gt; 
Adecco</value>
    </values>
    <values>
        <field>ADC_Email_Subject__c</field>
        <value xsi:type="xsd:string">Inquiry Created {!case.casenumber}</value>
    </values>
</CustomMetadata>
